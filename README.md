<p style="font-size: 18px">
Universidad de San Carlos de Guatemala
<br>
Facultad de Ingeniería
<br>
Escuela de Ciencias y Sistemas
<br>
Software avanzado
<br>
Ing. Marco Tulio Aldana Prillwitz
<br>
Aux. Mario Obed Morales Guitz
</p>

<br><br><br><br>

<h1 align="center" style="font-size: 40px; font-weight: bold;">Practicas</h1>

<br><br><br>

<div align="center">

|  Carnet   |              Nombre             |
| :-------: | :-----------------------------: |
| 201700965 | José Carlos I Alonzo Colocho    |
| 201700565 | Karla Julissa Ajtun Velasquez   |
| 201700972 | Davis Francisco Edward Enriquez | 

</div>

<br><br>

<h4 align="center" style="font-size: 18px; font-weight: bold;">Guatemala 2022</h4>

---