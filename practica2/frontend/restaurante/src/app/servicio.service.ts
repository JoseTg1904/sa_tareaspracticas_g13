import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  private apiRestaurante = 'http://localhost:3001';

  constructor(private http: HttpClient) { }

  avisarRepartidor(token: string, id: number){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiRestaurante}/avisarRepartidor/`+ String(id);
    return this.http.put<any>(path,{}, requestOptions);
  }

  verPedidoRestaurante(token: string){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiRestaurante}/pedidosRestaurante`;
    return this.http.get<any>(path, requestOptions);
  }
}
