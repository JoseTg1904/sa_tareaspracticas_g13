import requests
from flask import Flask, render_template, request, flash, redirect, url_for
app = Flask(__name__)
app.config['SECRET_KEY'] = b'_5#y2L"F4Q8z\n\xec]/'
@app.route("/", methods=["POST","GET"])# de esta forma le indicamos la ruta para acceder a esta pagina.


def home():
   
    if request.method == "POST":
        if request.form['submit_button'] == 'loguear':
            url = 'http://api-login:8080/login'
            usuario = request.form["user"]
            rol = request.form["rol"]
            data = {
                'usuario':usuario,
                'tipo':rol
            }
            dataUrl = data
            res = requests.post(url, data=data)
            if res.status_code == 200:
                respuesta = res.json()
                data = respuesta['token']
                if data == 'denied':
                    flash('ERROR NO SE PUDO INGRESAR, USUARIO INCORRECTO.')
                else:
                    url = ""
                    if dataUrl['tipo'] == "cliente":
                        url = "http://localhost:5001/home/{}/{}".format(dataUrl['usuario'], str(data))
                    elif dataUrl['tipo'] == "restaurante":
                        url = "http://localhost:5003/home/{}/{}".format(dataUrl['usuario'], str(data))
                    elif dataUrl['tipo'] == "repartidor":
                        url = "http://localhost:5002/home/{}/{}".format(dataUrl['usuario'], str(data))

                    return render_template('index.html', datos = {
                        'url': url
                    })
                    # flash('Tu token es: '+ str(data))
            else:
                flash('ERROR NO SE PUDO CONECTAR CON EL SERVIDOR')
            return render_template('index.html', datos = {
                        'url': ""
                    })
        return render_template('index.html', datos = {
                        'url': ""
                    })
    else:
        return render_template('index.html', datos = {
                        'url': ""
                    })

if __name__ == "__main__":
    app.run(debug=True, port=5000, host="0.0.0.0")