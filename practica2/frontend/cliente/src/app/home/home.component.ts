import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicioService } from '../servicio.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: string = '';
  token: string = '';
  comida = '';
  idPedido = 0;
  comidas : string[] = [];
  misPedidos : any[] = [];

  constructor(private rutaActiva: ActivatedRoute, private servicio: ServicioService) { 
  }

  ngOnInit(): void {
    this.user = this.rutaActiva.snapshot.paramMap.get('user') || '';
    this.token = this.rutaActiva.snapshot.paramMap.get('token') || '';
    console.log("usuario", this.user)
    console.log("token", this.token)
  }

  agregar(){
    this.comidas.push(this.comida);
    this.comida = '';
  }

  limpiar(){
    this.comidas = [];
  }

  enviarPedido(){
    this.servicio.enviarPedido(this.user, this.token, this.comidas).subscribe( data => {
      console.log(data)
      alert(data.estado);
      this.limpiar();
    },
    error => {
      alert(error.error.mensaje);
    });
  }

  pedidoPorIdRestaurante(){
    this.servicio.verPedidoRestaurante(this.token, this.idPedido).subscribe( data => {
      alert(data.estado);
    },
    error => {
      alert(error.error.mensaje);
    });
  }

  pedidoPorIdRepartidor(){
    this.servicio.verPedidoRepartidor(this.token, this.idPedido).subscribe( data => {
      alert(data.estado);
    },
    error => {
      alert(error.error.mensaje);
    }
    );
  }

  allPedidosRestaurante(){
    this.servicio.misPedidosRestaurante(this.user, this.token).subscribe( data => {
      data.forEach((element: any) => {
        element.pedido = element.pedido.pedido.join();
      });
      this.misPedidos = data;
    });
  }

  allPedidosRepartidor(){
    this.servicio.misPedidosRepartidor(this.user, this.token).subscribe( data => {
      data.forEach((element: any) => {
        element.pedido = element.pedido.pedido.join();
      });
      this.misPedidos = data;
    });
  }

}
