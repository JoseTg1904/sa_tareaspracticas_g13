import requests
import json
import jwt
import os

from flask import Flask, Response, request
from flask_cors import CORS
from datetime import datetime

app = Flask(__name__) # Inicializacion de la aplicación de Flask
app.config['SECRET_KEY'] = "secret"
CORS(app) # Habilitando la peticion de recursos cruzados

"""
    Conjunto de variables globales
"""
#Headers
cabecera = {
    "Accept"      : "application/json", 
    "Content-Type": "application/json"
}

#URL de los servicios
ruta = os.environ['rutaRepartidor']

pedidos = []
pathLog = os.environ['pathLog']

"""
    Metodos y funciones
"""
@app.route("/validarToken")
def validarToken():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'restaurante':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    return Response(json.dumps({'estado': 'token correcto'}), status = 200)

@app.route("/recibirPedido", methods = ["POST"])
def recibirPedido():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API restaurante")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API restaurante")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'restaurante':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API restaurante")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    pedido = {}
    pedido["pedido"] = request.json
    pedido["estado"] = "En proceso"
    pedido["id"] = len(pedidos)

    pedidos.append(pedido)
    escribirLog(request.path + " - Pedido agregado - API restaurante")

    return Response(json.dumps({"mensaje": "almacenado"}), status = 201)

@app.route("/informarEstado/<int:id>")
def informarEstado(id):
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API restaurante")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API restaurante")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'restaurante':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API restaurante")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    try:
        pedido = pedidos[id]
    except:
        escribirLog(request.path + " - Pedido no encontrado - API restaurante")
        return Response(json.dumps({'mensaje': 'pedido no encontrado'}), status = 404)
    else:
        mensaje = json.dumps({"estado": pedido["estado"] })
        escribirLog(request.path + " - Pedido encontrado - API restaurante")
        return Response(mensaje, status = 200)

@app.route("/avisarRepartidor/<int:id>", methods = ["PUT"])
def avisarRepartidor(id):
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API restaurante")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API restaurante")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'restaurante':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API restaurante")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    try:
        pedido = pedidos[id]
    except:
        escribirLog(request.path + " - Pedido no encontrado - API restaurante")
        return Response(json.dumps({'mensaje': 'pedido no encontrado'}), status = 404)
    else:
        pedido["estado"] = "Enviado al repartidor"
        escribirLog(request.path + " - Pedido transferido al repartidor - API restaurante")

        tokenEnviar = jwt.encode({"rol": "repartidor"}, "secret", algorithm="HS256")
        cabecera['Authorization'] = tokenEnviar
        peticion = requests.post(ruta + "recibirPedido", headers = cabecera, json = pedido["pedido"])

        if peticion.status_code == 201:
            return Response(json.dumps({ "estado": "entregado al repartidor" }), status = 200)

        escribirLog(request.path + " - Repartidor no disponible - API restaurante")
        return Response(json.dumps({ "estado": "repartidor no disponible" }), status = 400)

@app.route('/pedidosRestaurante', methods = ["GET"])
def pedidosRestaurante():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API restaurante")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API restaurante")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'restaurante':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API restaurante")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    escribirLog(request.path + " - Pedidos enviados - API restaurante")
    return Response(json.dumps(pedidos), status = 200)

@app.route('/pedidosCliente', methods = ["POST"])
def pedidosCliente():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API restaurante")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API restaurante")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'restaurante':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API restaurante")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    pedidosPropios = []
    nombre = request.json
    nombre = nombre['nombre']
    for pedido in pedidos:
        if pedido['pedido']['cliente'] == nombre:
            pedidosPropios.append(pedido)

    escribirLog(request.path + " - Pedidos del cliente " + nombre + " encontrados - API restaurante")
    return Response(json.dumps(pedidosPropios), status = 200)

def escribirLog(contenido):
    archivo = open(pathLog, 'a')
    archivo.write("[{}] {} \n".format(datetime.now(), contenido))
    archivo.close()

if __name__ == "__main__":
    app.run(port = 3001, debug = True, host = "0.0.0.0")