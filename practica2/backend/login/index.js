const express = require("express");
var bodyParser = require("body-parser");
const fs = require('fs');
const app = express();
const jwt = require("jsonwebtoken");
app.use(bodyParser.json({ limit: "10mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

var port = 8080;
app.listen(port, () => console.log("Server is running on port ", port));

app.post("/login", async (req, res) => {
  let dato = req.body;
  let tipo="f";
  if(String(dato.tipo).toLocaleLowerCase() == 'cliente'){
    tipo = 'cliente'
  }else if(String(dato.tipo).toLocaleLowerCase() == 'restaurante'){
      tipo = 'restaurante'
  }else if(String(dato.tipo).toLocaleLowerCase() == 'repartidor'){
      tipo = 'repartidor'
  }
  
  let path = process.env.pathLog || 'log.txt'
  if(tipo=="f"){
    let respuesta = {
        token: "denied"
    }
    let msj = "Usuario "+ dato.usuario+", Denegado.\n"
    fs.appendFile(path, msj, (err) => {
        if (err) throw err;
    });

      res.status(400)
      res.send(respuesta)
  }else{
    const accessToken = generateAccessToken(tipo, dato.usuario);
    let respuesta = {
      token: accessToken
    }
    
    let msj ="Usuario "+ dato.usuario+", Logueado, Token: "+accessToken+".\n"
    fs.appendFile(path, msj, (err) => {
        if (err) throw err;
    });
    res.status(200)
    res.send(respuesta)
  }
})

app.get("/getlog",  async (req, res) => {
    let path = process.env.pathLog || 'log.txt'
    fs.readFile(path, 'utf-8', (err, data) => {
        if(err) {
            res.send("ERROR AL LEER LOS DATOS")
        } else {
            res.send(data)
        }
    });
})

//generar token
function generateAccessToken(tipo, usuario){
    return jwt.sign({'rol': tipo, 'usuario': usuario}, "secret");
}

module.exports = app;