# Practica 2

## Frontend

Para esta practica se crearon cuatro frontends, de los cuales uno esta desrrollado en Python (Flask) y los otros tres en Angular

## Pruebas unitarias

También se desarrollaron pruebas unitarias para los microservicios que fueron creados durante la practica uno, para el caso de los que estaban realizados en python se utilizo la libreria __unittest__ que ya es parte de las librerias estandar que maneja Python 3 en su version 3.8.10 que fue la utilizada para el desarrollo de los mismos; a su vez para realizar las pruebas unitarias sobre Node JS y el sevidor generado con Express se utilizo __chai__.

## Jenkins

Para poder realizar el seguimiento del repositorio y asi poder automatizar los procesos que conllevan el ciclo devops, se hizo uso de un jenkisfile, en el cual se declaro toda la sintaxis necesaria para poder realizar el testeo, creacion y despliegue de lo que se estaba monitoreando; adentro de este archivo existen ciertas cosas remarcables

```jenkinsfile
def <nombre> = <valor>
```
con esto se pueden crear variables que nos pueden ser utilizar para tomar ciertas decisiones en los distintos pasos que componen el archivo

<br />

``` jenkinsfile
sh (script: "", returnStatus: boolean)
```
Con esto podemos obtener el estatus de salida del comando ejecutado, lo cual nos puede ser util para tomar decisiones y para evitar que ese __stage__ se __muera__ y haga que todo el pipeline se caiga.

<br />

```git
git diff --quiet HEAD^ -- <carpeta>
```

Con este comando podemos revisar si existen cambios en un directorio o archivo especifico respecto al commit anterior sobre la rama actual que se este ejecutando, este devuelve un __status__ que tiene valor 1 si hubieran cambios o 0 en caso contrario