import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  private apiRepartidor = 'http://localhost:3002';

  constructor(private http: HttpClient) { }

  marcarEntregado(token: string, id: number){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiRepartidor}/marcarEntregado/`+ String(id);
    return this.http.put<any>(path,{}, requestOptions);
  }

  verPedidoRepartidor(token: string){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiRepartidor}/pedidosRepartidor`;
    return this.http.get<any>(path, requestOptions);
  }
}
