import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicioService } from '../servicio.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: string = '';
  token: string = '';
  idPedido = 0;
  misPedidos : any[] = [];

  constructor(private rutaActiva: ActivatedRoute, private servicio: ServicioService) { }

  ngOnInit(): void {
    this.user = this.rutaActiva.snapshot.paramMap.get('user') || '';
    this.token = this.rutaActiva.snapshot.paramMap.get('token') || '';
  }

  entregar(){
    this.servicio.marcarEntregado(this.token, this.idPedido).subscribe( data => {
      alert(data.mensaje);
    },
    error => {
      alert(error.error.mensaje);
    });
  }

  verPedidos(){
    this.servicio.verPedidoRepartidor(this.token).subscribe( data => {
      data.forEach((element: any) => {
        element.pedido = element.pedido.pedido.join();
      });
      this.misPedidos = data;
    });
  }

}
