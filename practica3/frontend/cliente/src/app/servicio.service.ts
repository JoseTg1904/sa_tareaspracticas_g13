import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  private apiCliente = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  enviarPedido(cliente: string, token: string, pedido: string[]){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiCliente}/solicitarPedido`;
    return this.http.post<any>(path,{
      cliente:cliente,
      pedido: pedido
    }, requestOptions);
  }

  verPedidoRestaurante(token: string, id: number){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiCliente}/verificarRestaurante/`+ String(id);
    return this.http.get<any>(path, requestOptions);
  }

  verPedidoRepartidor(token: string, id: number){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiCliente}/verificarRepartidor/`+ String(id);
    return this.http.get<any>(path, requestOptions);
  }

  misPedidosRestaurante(cliente: string, token: string){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiCliente}/misPedidosRestaurante`;
    return this.http.post<any>(path,{
      nombre:cliente
    }, requestOptions);
  }

  misPedidosRepartidor(cliente: string, token: string){
    const headers = new HttpHeaders({
      'Authorization': `${token}`
    });
    
  const requestOptions = { headers: headers };
    const path = `${this.apiCliente}/misPedidosRepartidor`;
    return this.http.post<any>(path,{
      nombre:cliente
    }, requestOptions);
  }
}
