import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicioService } from '../servicio.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: string = '';
  token: string = '';
  idPedido = 0;
  misPedidos : any[] = [];

  constructor(private rutaActiva: ActivatedRoute, private servicio: ServicioService) { }

  ngOnInit(): void {
    this.user = this.rutaActiva.snapshot.paramMap.get('user') || '';
    this.token = this.rutaActiva.snapshot.paramMap.get('token') || '';
  }
  
  enviar(){
    this.servicio.avisarRepartidor(this.token, this.idPedido).subscribe( data => {
      alert(data.estado);
    },
    error => {
      alert(error.error.mensaje);
    });
  }

  verPedidos(){
    this.servicio.verPedidoRestaurante(this.token).subscribe( data => {
      data.forEach((element: any) => {
        element.pedido = element.pedido.pedido.join();
      });
      this.misPedidos = data;
    });
  }
}
