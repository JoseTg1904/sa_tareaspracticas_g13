import json
import jwt
import os

from flask import Flask, Response, request
from flask_cors import CORS
from datetime import datetime

app = Flask(__name__) # Inicializacion de la aplicación de Flask
app.config['SECRET_KEY'] = "secret" #clave para desencriptar el jwt
CORS(app) # Habilitando la peticion de recursos cruzados

"""
    Conjunto de variables globales
"""
pedidos = []
pathLog = os.environ['pathLog']

"""
    Metodos y funciones
"""
@app.route("/validarToken")
def validarToken():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'repartidor':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    return Response(json.dumps({'estado': 'token correcto'}), status = 200)

@app.route("/recibirPedido", methods = ["POST"])
def recibirPedido():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API repartidor")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API repartidor")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'repartidor':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API repartidor")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    pedido = {}
    pedido["pedido"] = request.json
    pedido["estado"] = "En ruta"
    pedido["id"] = len(pedidos)

    pedidos.append(pedido)
    escribirLog(request.path + " - Pedido insertado - API repartidor")

    return Response(json.dumps({"mensaje": "almacenado"}), status = 201)

@app.route("/informarEstado/<int:id>")
def informarEstado(id):
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API repartidor")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API repartidor")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'repartidor':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API repartidor")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    try:
        pedido = pedidos[id]
    except:
        escribirLog(request.path + " - Pedido no encontrado - API repartidor")
        return Response(json.dumps({'mensaje': 'pedido no encontrado'}), status = 404)
    else:
        mensaje = json.dumps({'estado': pedido["estado"] })
        escribirLog(request.path + " - Pedido encontrado - API repartidor")
        return Response(mensaje, status = 200)

@app.route("/marcarEntregado/<int:id>", methods = ["PUT"])
def avisarRepartidor(id):
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API repartidor")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API repartidor")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'repartidor':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API repartidor")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    try:
        pedido = pedidos[id]
    except:
        escribirLog(request.path + " - Pedido no econtrado - API repartidor")
        return Response(json.dumps({'mensaje': 'pedido no encontrado'}), status = 404)
    else:
        pedido["estado"] = "Entregado"

        escribirLog(request.path + " - Pedido entregado - API repartidor")
        return Response(json.dumps({ "mensaje": "pedido entregado" }), status = 200)

@app.route('/pedidosRepartidor', methods = ["GET"])
def pedidosRestaurante():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API repartidor")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API repartidor")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'repartidor':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API repartidor")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    escribirLog(request.path + " - Pedidos enviados - API repartidor")
    return Response(json.dumps(pedidos), status = 200)

@app.route('/pedidosCliente', methods = ["POST"])
def pedidosCliente():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API repartidor")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API repartidor")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'repartidor':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API repartidor")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    pedidosPropios = []
    print("aqui si")
    nombre = request.json
    nombre = nombre['nombre']
    for pedido in pedidos:
        if pedido['pedido']['cliente'] == nombre:
            pedidosPropios.append(pedido)

    escribirLog(request.path + " - Pedidos del cliente " + nombre + " encontrados - API repartidor")
    return Response(json.dumps(pedidosPropios), status = 200)


def escribirLog(contenido):
    archivo = open(pathLog, 'a')
    archivo.write("[{}] {} \n".format(datetime.now(), contenido))
    archivo.close()

if __name__ == "__main__":
    app.run(port = 3002, debug = True, host = "0.0.0.0")