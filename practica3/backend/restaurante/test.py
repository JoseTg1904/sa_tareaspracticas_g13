try:
    from restaurante import app
    import unittest
    import json
except Exception as e:
    print("Error al importar modulos.", e)

class FlaskTest(unittest.TestCase):

    def test_index(self):
        try:
            headers = {
                'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOiJyZXN0YXVyYW50ZSIsInVzdWFyaW8iOiJqdWxpc3NhIiwiaWF0IjoxNjU0NjM5NjM2fQ.WpKlpK7XVfeNKscINThs6nt8PCT2gMMYklvA5_QJYOI'
            }
            tester = app.test_client(self)
            response = tester.get("/validarToken", headers=headers)
            statuscode = response.status_code
            self.assertEqual(statuscode, 200)
            print("correcto")
        except:
            print("fallido")

if __name__ == "__main__":
    unittest.main()