from email import header
from hashlib import algorithms_available

try:
    from repartidor import app
    import unittest
except Exception as e:
    print("Error al importar modulos.", e)

class FlaskTest(unittest.TestCase):
    def test_index(self):
        headers = {
        'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOiJyZXBhcnRpZG9yIiwidXN1YXJpbyI6Imp1bGlzc2EiLCJpYXQiOjE2NTQ2MzE4MjJ9.RGqIFhYv5I5akWmQq6gXUZityZDmJ2aODl6wAWpK__E'
        }
        tester = app.test_client(self)
        response = tester.get("/pedidosRepartidos", headers=headers)
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

if __name__ == "__main__":
    unittest.main()