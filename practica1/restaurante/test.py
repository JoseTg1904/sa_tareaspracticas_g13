try:
    from restaurante import app
    import unittest
    import json
except Exception as e:
    print("Error al importar modulos.", e)

class FlaskTest(unittest.TestCase):
    def test_index(self):
        headers = {
        'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOiJyZXN0YXVyYW50ZSIsInVzdWFyaW8iOiJqdWxpc3NhIiwiaWF0IjoxNjU0NjM5NjM2fQ.WpKlpK7XVfeNKscINThs6nt8PCT2gMMYklvA5_QJYOI'
        }
        info={"pedido":"prueba"}
        tester = app.test_client(self)
        response = tester.post("/recibirPedido", headers=headers, data=json.dumps(info))
        statuscode = response.status_code
        self.assertEqual(statuscode, 201)

if __name__ == "__main__":
    unittest.main()