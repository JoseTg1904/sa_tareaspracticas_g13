## Practica 1

Para esta practica se realizaron 4 microservicios los cuales se detallan a continuación:

- Login

Para este microservicio se utilizó Node JS, para el JWS se utilizó la librería "jsonwebtoken" el cual se instaló por medio del comando: npm i jsonwebtoken.

El dockerfile utilizado para este microservicio es el siguiente:

```
FROM node:16.14.0
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD ["node", "index.js"]
```

- Cliente

Para este microservicio se utilizó Python, para el JWS se utilizó la librería "Pyjwt" el cual se instaló por medio del comando: pip install PyJWT.


El dockerfile utilizado para este microservicio es el siguiente:

```
FROM python:3.8.10
WORKDIR /code
COPY requirements.txt /tmp/requirements.txt
RUN python3 -m pip install -r /tmp/requirements.txt
RUN mkdir /code/log
COPY . .
CMD ["python", "./cliente.py"]
EXPOSE 3000
```
- Restaurante


Para este microservicio se utilizó Python, para el JWS se utilizó la librería "Pyjwt" el cual se instaló por medio del comando: pip install PyJWT.

El dockerfile utilizado para este microservicio es el siguiente:

```
FROM python:3.8.10
WORKDIR /code
COPY requirements.txt /tmp/requirements.txt
RUN python3 -m pip install -r /tmp/requirements.txt
RUN mkdir /code/log
COPY . .
CMD ["python", "./restaurante.py"]
EXPOSE 3001
```
- Repartidor

Para este microservicio se utilizó Python, para el JWS se utilizó la librería "Pyjwt" el cual se instaló por medio del comando: pip install PyJWT.

El dockerfile utilizado para este microservicio es el siguiente:

```
FROM python:3.8.10
WORKDIR /code
COPY requirements.txt /tmp/requirements.txt
RUN python3 -m pip install -r /tmp/requirements.txt
RUN mkdir /code/log
COPY . .
CMD ["python", "./repartidor.py"]
EXPOSE 3002
```

- Docker-compose
Se utilizó docker compose para realizar la comunicación de los Dockerfile creados para cada microservicio.

Para el docker-compose se utilizo lo siguiente:

```
version: "3.9"
services:
  api-login:
    build: ./LoginService/
    ports:
      - 8080:8080
    environment:
      - pathLog=/app/log/log.log
    volumes:
      - ./:/app/log
  api-cliente:
    build: ./cliente/
    ports: 
        - 3000:3000
    environment: 
      - pathLog=/code/log/log.log
      - rutaRestaurante=http://api-restaurante:3001/
      - rutaRepartidor=http://api-repartidor:3002/
    volumes:
      - ./:/code/log
  api-repartidor:
    build: ./repartidor/
    ports: 
        - 3002:3002
    environment: 
      - pathLog=/code/log/log.log
    volumes:
      - ./:/code/log
  api-restaurante:
    build: ./restaurante/
    ports: 
        - 3001:3001
    environment: 
      - pathLog=/code/log/log.log
      - rutaRepartidor=http://api-repartidor:3002/
    volumes:
      - ./:/code/log
```
