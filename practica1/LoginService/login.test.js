const app = require("./index.js"); // Link to your server file
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
chai.use(chaiHttp);

describe('Testing API REST', function(){  
    describe('Creando Token: ',()=>{
    it('Debería de crear un token.', (done) => {
    chai.request(app)
    .post('/login')
    .send({ usuario: "prueba", tipo: "cliente"})
    .end( function(err,res){
        if (err){
            console.log(err);
        } else {
            tok = res.body
            expect(res).to.have.status(200);
            expect(tok.token).to.not.equal("denied");
            done();
        }
    });
    });
    });   
});