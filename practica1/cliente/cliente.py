import requests
import json
import jwt
import os

from flask import Flask, Response, request
from flask_cors import CORS
from datetime import datetime

app = Flask(__name__) # Inicializacion de la aplicación de Flask
app.config['SECRET_KEY'] = "secret"
CORS(app) # Habilitando la peticion de recursos cruzados

"""
    Conjunto de variables globales
"""
#Headers
cabecera = {
    "Accept"      : "application/json", 
    "Content-Type": "application/json"
}

#URL del restaurante
rutaRestaurante = os.environ['rutaRestaurante']
rutaRepartidor  = os.environ['rutaRepartidor']

pedidos = []
pathLog = os.environ['pathLog']
"""
    Metodos y funciones
"""
@app.route("/solicitarPedido", methods = ["POST"])
def solicitarPedido():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'cliente':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    tokenEnviar = jwt.encode({"rol": "restaurante"}, "secret", algorithm="HS256")
    cabecera['Authorization'] = tokenEnviar
    peticion = requests.post(rutaRestaurante + "recibirPedido", 
    headers = cabecera, json = request.json)

    if peticion.status_code == 201:
        escribirLog(request.path + " - Pedido envido al restaurante - API cliente")
        return Response(json.dumps({ "estado": "pedido enviado al restaurante" }), status = 201)

    escribirLog(request.path + " - Restaurante no disponible - API cliente")
    return Response(json.dumps({ "estado": "restaurante no disponible" }), status = 400)

@app.route("/verificarRestaurante/<int:id>")
def verificarRestaurante(id):
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'cliente':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    tokenEnviar = jwt.encode({"rol": "restaurante"}, "secret", algorithm="HS256")
    cabecera['Authorization'] = tokenEnviar
    peticion = requests.get(rutaRestaurante + "informarEstado/" + str(id),
    headers = cabecera)

    if peticion.status_code == 200:
        escribirLog(request.path + " - Pedido encontrado y devuelto - API cliente")
        return Response(json.dumps(peticion.json()), status = 200)

    escribirLog(request.path + " - pedido no encontrado en restaurante - API cliente")
    return Response(json.dumps(peticion.json()), status = 404)

@app.route("/verificarRepartidor/<int:id>")
def verificarRepartidor(id):
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'cliente':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    tokenEnviar = jwt.encode({"rol": "repartidor"}, "secret", algorithm="HS256")
    cabecera['Authorization'] = tokenEnviar
    peticion = requests.get(rutaRepartidor + "informarEstado/" + str(id),
    headers = cabecera)

    if peticion.status_code == 200:
        escribirLog(request.path +" - pedido encontrado en repartidor - API cliente")
        return Response(json.dumps(peticion.json()), status = 200)

    escribirLog(request.path +" - pedido no encontrado en repartidor - API cliente")
    return Response(json.dumps(peticion.json()), status = 404)

@app.route("/misPedidosRestaurante", methods = ["POST"])
def misPedidosRestaurante():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'cliente':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    tokenEnviar = jwt.encode({"rol": "restaurante"}, "secret", algorithm="HS256")
    cabecera['Authorization'] = tokenEnviar
    peticion = requests.post(rutaRestaurante + "pedidosCliente", 
    headers = cabecera, json = request.json)

    escribirLog(request.path + " - pedidos del cliente retornados - API cliente")
    return Response(json.dumps(peticion.json()), status = 200)

@app.route("/misPedidosRepartidor", methods = ["POST"])
def misPedidosRepartidor():
    if 'Authorization' not in request.headers:
        escribirLog(request.path + " - token no encontrado - API cliente")
        return Response(json.dumps({ "estado": "token no encontrado" }), status = 400)
   
    token = request.headers['Authorization']
    decodificado = jwt.decode(token, app.config['SECRET_KEY'])
    if 'rol' not in decodificado:
        escribirLog(request.path +" - token invalido - API cliente")
        return Response(json.dumps({ "estado": "token invalido" }), status = 400)

    if decodificado['rol'] != 'cliente':
        escribirLog(request.path + " - Sin autorizacion para esta accion - API cliente")
        return Response(json.dumps({ "estado": "sin autorizacion para esta accion" }), status = 401)

    tokenEnviar = jwt.encode({"rol": "repartidor"}, "secret", algorithm="HS256")
    cabecera['Authorization'] = tokenEnviar
    peticion = requests.post(rutaRepartidor + "pedidosCliente", 
    headers = cabecera, json = request.json)

    escribirLog(request.path + " - pedidos del cliente retornados - API cliente")
    return Response(json.dumps(peticion.json()), status = 200)

def escribirLog(contenido):
    archivo = open(pathLog, 'a')
    archivo.write("[{}] {} \n".format(datetime.now(), contenido))
    archivo.close()

if __name__ == "__main__":
    app.run(port = 3000, debug = True, host = "0.0.0.0")