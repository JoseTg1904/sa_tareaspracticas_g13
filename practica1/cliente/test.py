try:
    from cliente import app
    import unittest
    import json
except Exception as e:
    print("Error al importar modulos.", e)

class FlaskTest(unittest.TestCase):
    def test_index(self):
        headers = {
        'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2wiOiJjbGllbnRlIiwidXN1YXJpbyI6Imp1bGlzc2EiLCJpYXQiOjE2NTQ2NDA5NTF9.gt5cObFCt_kCZdEo99r_i5OMCOBZsndL7NrHU47r_dM'
        }
        info={
            "cliente": "prueba", 
            "pedido": "hamburguesa"
        }

        tester = app.test_client(self)
        response = tester.post("/solicitarPedido", headers=headers, data=json.dumps(info))
        statuscode = response.status_code
        self.assertEqual(statuscode, 400)

if __name__ == "__main__":
    unittest.main()